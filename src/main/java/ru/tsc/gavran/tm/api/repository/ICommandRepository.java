package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandNames();

    Collection<String> getCommandArg();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);

}