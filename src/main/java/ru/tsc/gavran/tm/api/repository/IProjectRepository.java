package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project findByName(String name);

    Project removeByName(String name);

    Project startById(String id);

    Project startByName(String name);

    Project startByIndex(int index);

    Project finishById(String id);

    Project finishByName(String name);

    Project finishByIndex(int index);

    Project changeStatusById(String id, Status status);

    Project changeStatusByName(String name, Status status);

    Project changeStatusByIndex(int index, Status status);


}