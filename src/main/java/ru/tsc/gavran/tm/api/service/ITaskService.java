package ru.tsc.gavran.tm.api.service;

import ru.tsc.gavran.tm.api.IService;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Task;

public interface ITaskService extends IService<Task> {

    void create(String name);

    void create(String name, String description);

    Task changeStatusById(String id, Status status);

    Task changeStatusByName(String name, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task findByName(String name);

    Task removeByName(String name);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task startById(String id);

    Task startByName(String name);

    Task startByIndex(Integer index);

    Task finishById(String id);

    Task finishByName(String name);

    Task finishByIndex(Integer index);

}