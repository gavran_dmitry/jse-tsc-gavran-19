package ru.tsc.gavran.tm.command.user;

import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;

public class UserClearCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear all users.";
    }

    @Override
    public void execute() {
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAdmin) throw new AccessDeniedException();
        serviceLocator.getUserService().clear();
    }
}