package ru.tsc.gavran.tm.api.service;

import ru.tsc.gavran.tm.api.IService;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Project;

public interface IProjectService extends IService<Project> {

    void create(String name);

    void create(String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByName(String name, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project findByName(String name);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project startById(String id);

    Project startByName(String name);

    Project startByIndex(Integer index);

    Project finishById(String id);

    Project finishByName(String name);

    Project finishByIndex(Integer index);

}